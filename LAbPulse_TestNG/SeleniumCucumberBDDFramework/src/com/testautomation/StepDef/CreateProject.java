package com.testautomation.StepDef;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateProject {
	
	public static WebDriver driver;
	@Given("^User is on Labpulse home page$")
	public void user_is_on_Labpulse_home_page() throws Exception
	{
//		Robot robot = new Robot();
//		robot.keyPress(KeyEvent.VK_ESCAPE);
//		robot.keyRelease(KeyEvent.VK_ESCAPE);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	  @When("^Enter on lab project flow$") 
	  public void enter_on_lab_project_flow()throws Exception
	  { 
//		  Robot r=new Robot(); 
//		  r.keyPress(KeyEvent.VK_ESCAPE);
//		  r.keyRelease(KeyEvent.VK_ESCAPE);
	  
		  driver.findElement(By.xpath("createProject")).click(); 
		  Thread.sleep(2000);
	  }
	  
	  
	  @Then("^Enters '(.*)' and '(.*)' and '(.*)' and '(.*)'$") 
	  public void enters_ProjectTitle_and_Sponsor_and_Budget_and_Objective(String s1, String s2, String s3, String s4 ) throws AWTException, Exception
	  {
		  driver.findElement(By.xpath("projectTitle_textBox_Xpath")).sendKeys(s1);
		  Thread.sleep(2000);
		  		  
		  driver.findElement(By.xpath("objective_textBox_Xpath")).sendKeys(s4);
		  
		  driver.findElement(By.xpath("budget_textBox")).sendKeys(s3);
		  
		  Thread.sleep(2000);
		  Robot r1=new Robot();
		  r1.keyPress(KeyEvent.VK_ESCAPE);
		  r1.keyRelease(KeyEvent.VK_ESCAPE);
		  
		  
	  
		  Robot r=new Robot(); 
		  // r.keyPress(KeyEvent.VK_ESCAPE); 
		  // r.keyRelease(KeyEvent.VK_ESCAPE);
		  driver.findElement(By.xpath("sponsor_textBox")).sendKeys(s2);
		  Thread.sleep(3000); r.keyPress(KeyEvent.VK_ENTER);
		  r.keyRelease(KeyEvent.VK_ENTER); Thread.sleep(4000);
		  r.keyPress(KeyEvent.VK_TAB); 
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB); 
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB); 
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB); 
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB);
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_ENTER);
		  r.keyRelease(KeyEvent.VK_ENTER);
		  //driver.findElement(By.xpath(select_Sponsor)).click();
	  }
	 
//--------------------------------------------------------------------------------------
/*	 

	 //driver.findElement(By.xpath(Sponsor_TB)).click();
	  	r.keyPress(KeyEvent.VK_TAB);
	  	r.keyRelease(KeyEvent.VK_TAB);
	  	r.keyPress(KeyEvent.VK_TAB); 
	  	r.keyRelease(KeyEvent.VK_TAB);
	  	r.keyPress(KeyEvent.VK_TAB);
	  	r.keyRelease(KeyEvent.VK_TAB);
	  
		driver.findElement(By.xpath(new_acc_sponsor)).click();
	  //r.keyPress(KeyEvent.VK_ENTER); //r.keyRelease(KeyEvent.VK_ENTER);
	 
	 	driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	  	driver.findElement(By.xpath(sponsor_checkbox)).click();
	 	driver.findElement(By.xpath(next_button)).click(); Thread.sleep(3000);
	 
	  	driver.findElement(By.xpath(AccName_Textbox)).sendKeys("Pushpendra");
	  	driver.findElement(By.xpath(Save_button)).click();

 */
//------------------------------------------------------------------------------  

	
	@Then("^Choose StartDate EndDate and Status$")
	 public void choose_StartDate_EndDate_and_Status()
	 {
		 driver.findElement(By.xpath("startDate")).sendKeys("Oct 4, 2019");
		 driver.findElement(By.xpath("endDate")).sendKeys("Oct 4, 2019");
	 }
	  
	  @Then("^Save the project info$")
	  public void save_the_project_info() throws Exception
	  {
		  Thread.sleep(8000);
		  driver.findElement(By.xpath("next_button_ProjectInfo")).click();
	  }
	  
	  
	  @Given("^User is on Project Team window$") 
	  public void user_is_on_Project_Team_window() throws Exception 
	  { 
		  Thread.sleep(3000);
	  }
	  
	  @When("^Fill the inputs$") public void fill_the_inputs() {
	  
	  }
	  
	  
	  @Then("^Enter '(.*)' and '(.*)'$") public void
	  Enter_ProjectTeam_and_Researcher(String p, String r1) throws Exception
	  {
	  	  driver.findElement(By.xpath("//input[@id='input-138']")).sendKeys(p);
		  driver.findElement(By.xpath("//input[@id='input-142']")).sendKeys(r1);
	  
		  Robot r=new Robot(); Thread.sleep(2000); r.keyPress(KeyEvent.VK_ENTER);
		  r.keyRelease(KeyEvent.VK_ENTER); Thread.sleep(3000);
		  r.keyPress(KeyEvent.VK_TAB); r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB); r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB); r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB); r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB); r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_ENTER); r.keyRelease(KeyEvent.VK_ENTER); 
	  }
	  
	  
	  @Then("^Save the project team info$") 
	  public void Save_the_project_team_info() throws Exception
	  { 
		  Thread.sleep(3000);
		  driver.findElement(By.xpath("//*[@id=\"content_691:0\"]/div[2]/form/button")).click();
	  }
	  
	  
	  @Given("^User is on Experiment Design window$") 
	  public void user_is_on_Experiment_Design_window()
	  {
	  
	  }
	  
	  @When("^Fill the inputs for exp$")
	  public void fill_the_inputs_for_exp() throws Exception
	  { 
		  Thread.sleep(3000);
	  }
	  
	  @Then("^User Enters '(.*)' and '(.*)'$") 
	  public void user_Enters_ExperimentName_and_Protocol(String p12, String r12) throws Exception
	  {
		  driver.findElement(By.xpath("//input[@id='input-181']")).sendKeys(p12);
		  driver.findElement(By.xpath("//input[@id='input-197']")).sendKeys(r12);
	  
		  Robot r=new Robot();
		  Thread.sleep(2000);
		  r.keyPress(KeyEvent.VK_ENTER);
		  r.keyRelease(KeyEvent.VK_ENTER); 
		  Thread.sleep(3000);
		  
		  r.keyPress(KeyEvent.VK_TAB);
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB);
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB);
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB);
		  r.keyRelease(KeyEvent.VK_TAB);
	  
		  r.keyPress(KeyEvent.VK_ENTER);
		  r.keyRelease(KeyEvent.VK_ENTER); 
	  }
	  
	  @Then("^Save the Experiment info$")
	  public void save_the_Experiment_info() throws Exception
	  { 
		  Thread.sleep(3000);
		  driver.findElement(By.xpath("//*[@id=\"content_691:0\"]/div[2]/form/button")).click();
	  }
	  
	  
	  @Given("^User is on Specimen Selection window$") public void
	  user_is_on_Specimen_Selection_window() 
	  {
	  
	  }
	  
	  @When("^Fill the inputs for Specimen Selection$") public void
	  fill_the_inputs_for_Specimen_Selection() 
	  {
	  
	  }
	  
	  @Then("^Required input is '(.*)'$") 
	  public void required_input_is_SpecimenName(String qk) throws Exception 
	  {
		  driver.findElement(By.xpath("//*[@id=\"input-239\"]")).sendKeys(qk);
	  
		  WebElement dd=driver.findElement(By.xpath("//input[@name='Nature__c']"));
		  dd.click(); dd.sendKeys(Keys.DOWN);
		  dd.sendKeys(Keys.DOWN);
		  dd.sendKeys(Keys.ENTER);
	  
		  driver.findElement(By.xpath("//input[@id=\"input-241\"]")).sendKeys("Oct 1, 2019"); //WebElement
		  dd=driver.findElement(By.xpath("//input[@id=\"input-241\"]"));
		  //dd2.click(); //dd2.sendKeys(Keys.ENTER);
	  
		  WebElement dd1=driver.findElement(By.xpath("//input[@name='Location__c']"));
		  dd1.click(); 
		  dd1.sendKeys(Keys.DOWN); 
		  dd1.sendKeys(Keys.DOWN);
		  dd1.sendKeys(Keys.ENTER); 
	  }
	  
	  
	  
	  @Then("^Save the Specimen info$")
	  public void save_the_Specimen_info() throws Exception
	  { 
		  Thread.sleep(4000);
		  driver.findElement(By.xpath("//*[@name='save']")).click();
	  }

	

}
