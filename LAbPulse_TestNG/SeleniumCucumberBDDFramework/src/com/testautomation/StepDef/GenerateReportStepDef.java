package com.testautomation.StepDef;

import static org.testng.Assert.assertTrue;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;
import com.testautomation.Listeners.ExtentReportListener;
import com.testautomation.Utility.PropertiesFileReader;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GenerateReportStepDef extends ExtentReportListener{
	
	public  WebDriver driver;
	PropertiesFileReader obj= new PropertiesFileReader();

	ExtentTest logInfo;
	//ExtentTest logInfo;
	@Given("^User is on Labpulse Home Page$")
	public void user_is_on_Labpulse_Home_Page() throws Exception
	{
		
		try {
			test = extent.createTest(Feature.class, "Graphical report generation");							
			test=test.createNode(Scenario.class, "Generate Report");						
			logInfo=test.createNode(new GherkinKeyword("Given"), "user_is_on_Labpulse_Home_Page");
		
			//Thread.sleep(1000);
			System.out.println("Hii There");
		
		logInfo.pass("Opened chrome browser and entered url");
		logInfo.addScreenCaptureFromPath(captureScreenShot(driver));			
	} catch (AssertionError | Exception e) 
		{
		
		testStepHandle("FAIL",driver,logInfo,e);			
	}		
	
	}
	
	
	@When("^User select Data Analysis$")
	public void user_select_Data_Analysis() throws Exception
	{
		try {
			logInfo=test.createNode(new GherkinKeyword("When"), "user_select_Data_Analysis");
			Robot r3 = new Robot();
		//driver.findElement(By.xpath("(//td/span/div)[8]")).click();
			Thread.sleep(3000);
			r3.keyPress(KeyEvent.VK_ESCAPE);
			r3.keyRelease(KeyEvent.VK_ESCAPE);
		
		r3.mouseMove(1144,405);
		Thread.sleep(3000);
		int mask = InputEvent.BUTTON1_DOWN_MASK;
		r3.mousePress(mask);
		r3.mouseRelease(mask);
		
		Thread.sleep(5000);
		r3.keyPress(KeyEvent.VK_ESCAPE);
		r3.keyRelease(KeyEvent.VK_ESCAPE);

		driver.findElement(By.xpath("//input[@name='Researcher']")).sendKeys("P");
		
		Thread.sleep(2000);
		r3.keyPress(KeyEvent.VK_ENTER);
		r3.keyRelease(KeyEvent.VK_ENTER);

		driver.findElement(By.xpath("//input[@name='Project']")).sendKeys("En");
		Thread.sleep(2000);
		r3.keyPress(KeyEvent.VK_ENTER);
		r3.keyRelease(KeyEvent.VK_ENTER);
		
		
		driver.findElement(By.xpath("//input[@name='Experiment']")).sendKeys("Co");
		Thread.sleep(2000);
		r3.keyPress(KeyEvent.VK_ENTER);
		r3.keyRelease(KeyEvent.VK_ENTER);
		
		driver.findElement(By.xpath("//button[@type='submit'][1]")).click();
		Thread.sleep(2000);
		
		assertTrue(driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div[2]/div/div/div/div[2]/div[1]/div/table/tr[2]/td[2]")).getText().matches("Mice1"));
		System.out.println("Result generated");
		
		logInfo.pass("Opened chrome browser and entered url");
		logInfo.addScreenCaptureFromPath(captureScreenShot(driver));			
				
		} catch (AssertionError | Exception e) {
			testStepHandle("FAIL",driver,logInfo,e);			
		
		}
		
	}
	
	/* @Then("^User Enters '(.*)' and '(.*)' and '(.*)'$")
	  public void user_Enters_Researcher_and_Project_and_Experiment(String Res, String Pro,String Ex) throws Exception 
	  {
	 
	  
	  
	  
	  
	  }
*/	 
	
	@Then("Generate Report")
	public void generate_Report() throws Exception
	{
		try {
			logInfo=test.createNode(new GherkinKeyword("Then"), "Generate Report");
			//Properties properties =  obj.getProperty(); 
		
		
		
		driver.findElement(By.xpath("//button[@type='submit'][1]")).click();
		Thread.sleep(10000);
		
		logInfo.pass("Opened chrome browser and entered url");
		logInfo.addScreenCaptureFromPath(captureScreenShot(driver));			
		
	} catch (AssertionError | Exception e) {
		testStepHandle("FAIL",driver,logInfo,e);			
	
	}
	}

	@Then("Reset")
	public void reset() 
	{
		
		try {
			logInfo=test.createNode(new GherkinKeyword("Then"), "Reset");
			//Properties properties =  obj.getProperty(); 
		driver.findElement(By.xpath("//button[@type='submit'][2]")).click();
		logInfo.pass("Opened chrome browser and entered url");
		logInfo.addScreenCaptureFromPath(captureScreenShot(driver));			
		
	} catch (AssertionError | Exception e) {
		testStepHandle("FAIL",driver,logInfo,e);			
	
	}
	}


}
