package com.testautomation.StepDef;


import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;
import com.testautomation.Listeners.ExtentReportListener;
import com.testautomation.Utility.BrowserUtility;
import com.testautomation.Utility.PropertiesFileReader;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginStepDef  extends ExtentReportListener{

	public static WebDriver driver;
	PropertiesFileReader obj= new PropertiesFileReader();

	ExtentTest logInfo=null;
	
	@Given("^user is on login window$")
	public void user_is_on_login_window()
	{
		
		try {
			test = extent.createTest(Feature.class, "Labpulse Login");							
			test=test.createNode(Scenario.class, "Login as a authenticated user");						
			logInfo=test.createNode(new GherkinKeyword("Given"), "user_is_on_login_window");
			Properties properties =  obj.getProperty(); 		
			driver=BrowserUtility.OpenBrowser(driver, properties.getProperty("browser.name"), properties.getProperty("browser.baseURL"));
	
			logInfo.pass("Opened chrome browser and entered url");
			logInfo.addScreenCaptureFromPath(captureScreenShot(driver));			
			
		} catch (AssertionError | Exception e) {
			testStepHandle("FAIL",driver,logInfo,e);			
		}		

	}

	@When("^enters '(.*)' and '(.*)'$")
	public void enters_username_and_Password(String arg1, String arg2)
	{
		
		
		try {
			logInfo=test.createNode(new GherkinKeyword("When"), "enters_username_and_Password");
			//Properties properties =  obj.getProperty(); 
			
			
			driver.findElement(By.id("username")).sendKeys(arg1);
			driver.findElement(By.id("password")).sendKeys(arg2);
			
			logInfo.pass("Correct username and password entered");
			logInfo.addScreenCaptureFromPath(captureScreenShot(driver));			
			
		} catch (AssertionError | Exception e) {
			testStepHandle("FAIL",driver,logInfo,e);			
		
		}
		
	}

	@Then("^click on login$")
	public void click_on_login() throws Exception 
	{
		try {
			logInfo=test.createNode(new GherkinKeyword("Then"), "click_on_login()");
			
					
		// Clicking on remember me
		driver.findElement(By.xpath("//*[@id=\"rememberUn\"]")).click();

		// Clicking on Login button
		driver.findElement(By.id("Login")).click();
		Thread.sleep(6000);
		
			
		logInfo.pass("user logged in successfully");
		logInfo.addScreenCaptureFromPath(captureScreenShot(driver));			
		
	} catch (AssertionError | Exception e) {
		testStepHandle("FAIL",driver,logInfo,e);			
	
	}
		
		
	}
	
}
