package com.testautomation.StepDef;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;
import com.testautomation.Listeners.ExtentReportListener;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateResult extends ExtentReportListener{
	public static WebDriver driver;
	ExtentTest logInfo=null;
	
	@Given("^User is on Home Page$")
	public void user_is_on_Home_Page() throws Exception
	{
		try {
			test = extent.createTest(Feature.class, "Create Results");							
			test=test.createNode(Scenario.class, "Create multiple Results by adding proper data set");						
			logInfo=test.createNode(new GherkinKeyword("Given"), "user_is_on_Labpulse_Home_Page");
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ESCAPE);
		robot.keyRelease(KeyEvent.VK_ESCAPE);
		
		
		logInfo.pass("Opened chrome browser and entered url");
		logInfo.addScreenCaptureFromPath(captureScreenShot(driver));			
		
	} catch (AssertionError | Exception e) 
		{
		testStepHandle("FAIL",driver,logInfo,e);			
	}		
	
	}
	
	
	@When("^User select Data Analysis to create result$")
	public void user_select_Data_Analysis_to_create_result() throws Exception
	{
		try {
			logInfo=test.createNode(new GherkinKeyword("When"), "user_select_Data_Analysis");
			
		
		
		driver.findElement(By.xpath("(//td/span/div)[8]")).click();
		
		Robot r3 = new Robot();
//		r3.mouseMove(1144,405);
//		Thread.sleep(3000);
//		int mask = InputEvent.BUTTON1_DOWN_MASK;
//		r3.mousePress(mask);
//		r3.mouseRelease(mask);
//		
		Thread.sleep(5000);
		r3.keyPress(KeyEvent.VK_ESCAPE);
		r3.keyRelease(KeyEvent.VK_ESCAPE);
		
		logInfo.pass("Opened chrome browser and entered url");
		logInfo.addScreenCaptureFromPath(captureScreenShot(driver));			
		
	} catch (AssertionError | Exception e) {
		testStepHandle("FAIL",driver,logInfo,e);			
	}		
		}
	
	
	
	
	
	
	@Then("^User Enters '(.*)' and '(.*)' and '(.*)' and '(.*)'$")
	public void user_Enters_ExperimentalDrug_and_Strains_and_AnimalName_and_Value(String Ex, String Str, String An,String Val) throws Exception
	{
		try {
			logInfo=test.createNode(new GherkinKeyword("Then"), "user_Enters_ExperimentalDrug_and_Strains_and_AnimalName_and_Value");
			
			
		driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div[2]/div/div/div/div/div/form/div/div[5]/button/lightning-icon")).click();
		
		
		WebElement dd5=driver.findElement(By.xpath("//div//input [@class='slds-input slds-combobox__input'][@placeholder='Search Experiments...']"));
		dd5.click(); 
		Thread.sleep(2000);
		  dd5.sendKeys(Keys.DOWN); 
		  dd5.sendKeys(Keys.DOWN);
		  dd5.sendKeys(Keys.ENTER);
		
		  WebElement dd1=driver.findElement(By.xpath("//div//input [@class='slds-input slds-combobox__input'][@placeholder='Search Lab Projects...']"));
		  dd1.click(); 
		  Thread.sleep(2000);
		  dd1.sendKeys(Keys.DOWN); 
		  dd1.sendKeys(Keys.DOWN);
		  dd1.sendKeys(Keys.ENTER); 
		
		String x;

		
			for (int k = 1, j = 0; k <= 4; k++) {
				for (int l = 1; l <= 4; l++) {
					for (int i = 1; i <= 4; i = i + 1) {

						String animal = ("Mice" + i);

						String strain[] = { "Balb/c", "Out bred", "C/57", "Gg4", "Ogk" };

						String drug = ("LDA" + k);

						String value = ("0.25" + i);

						j++;

						x = "(//td/input [@class=\"slds-input input uiInput uiInputText uiInput--default uiInput--input\"])["+ j + "]";
						driver.findElement(By.xpath(x)).sendKeys(drug);
						System.out.println(x + ": " + value);
						j++;

						x = "(//td/input [@class=\"slds-input input uiInput uiInputText uiInput--default uiInput--input\"])["+ j + "]";
						driver.findElement(By.xpath(x)).sendKeys(strain[l]);
						System.out.println(x + ": " + value);
						j++;

						x = "(//td/input [@class=\"slds-input input uiInput uiInputText uiInput--default uiInput--input\"])["+ j + "]";
						driver.findElement(By.xpath(x)).sendKeys(animal);
						System.out.println(x + ": " + value);
						j++;

						x = "(//td/input [@class=\"slds-input input uiInput uiInputText uiInput--default uiInput--input\"])["+ j + "]";
						//System.out.println(x + ": " + value);
						driver.findElement(By.xpath(x)).sendKeys(value);
						
						
						((JavascriptExecutor)driver).executeScript("scroll(0,80)");
						driver.findElement(By.xpath("(//*[contains(@data-key,'add')])[2]")).click();
												
					}
				}
			}
		
			Robot r3 = new Robot();
			r3.mouseMove(1201,700);
			//Thread.sleep(3000);
			int mask = InputEvent.BUTTON1_DOWN_MASK;
			r3.mousePress(mask);
			r3.mouseRelease(mask);
			
			//driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div[3]/div/div/table/tbody/tr[65]/td[6]")).click();
			
			Thread.sleep(5000);
			driver.findElement(By.xpath("//button[@class='slds-button slds-button_brand slds-align_absolute-center']")).click();
	
			logInfo.pass("Opened chrome browser and entered url");
			logInfo.addScreenCaptureFromPath(captureScreenShot(driver));			
			
		} catch (AssertionError | Exception e) {
			testStepHandle("FAIL",driver,logInfo,e);			
		}		
			
		}
}
