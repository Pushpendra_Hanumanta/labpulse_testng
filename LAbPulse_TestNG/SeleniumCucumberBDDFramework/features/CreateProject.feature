@CucumberHooks
Feature: Login into LabPulse

@TestCase4
	Scenario Outline: Create Lab Project
    Given User is on Labpulse home page
    When Enter on lab project flow
   Then Enters '<ProjectTitle>' and '<Sponsor>' and '<Budget>' and '<Objective>'
    Then Choose StartDate EndDate and Status  
    Then Save the project info
   
Examples:
|ProjectTitle											 |Sponsor		| Budget	|Objective																									|
|Agarose Gel Electrophoresis for resolving DNA fragments|GEF			|564	 	|seperation of DNA fragments was done by electrophoresis through agarose gels  at 70 volts in TBE.			|
    
Scenario Outline: Create Project Team
    Given User is on Project Team window
    When Fill the inputs
    Then Enter '<ProjectTeam>' and '<Researcher>'
   	Then Save the project team info
   
Examples:    
|ProjectTeam		|   Researcher		|
|Agarose_team		|	Pushpendara		|

Scenario Outline: Create Experiments
    Given User is on Experiment Design window
    When Fill the inputs for exp
    Then User Enters '<ExperimentName>' and '<Protocol>' 
   	Then Save the Experiment info
   
Examples:    
|ExperimentName				| Protocol			|
| DNA fragmentation			|Cloning Protocol	|

Scenario Outline: Create Specimen
    Given User is on Specimen Selection window
    When Fill the inputs for Specimen Selection
    Then Required input is '<SpecimenName>' 
    Then Save the Specimen info
   
Examples:    
|SpecimenName	|
|	leo			|