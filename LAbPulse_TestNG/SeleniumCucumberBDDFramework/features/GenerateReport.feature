@CucumberHooks
Feature: Graphical report generation

@TestCase6
Scenario Outline: Generate Report
    Given User is on Labpulse Home Page
    When User select Data Analysis
    Then User Enters '<Researcher>' and '<Project>' and '<Experiment>'
    Then Generate Report
    Then Reset
    
Examples:    
|Researcher	|Project	|Experiment		|
|push		|Enzyme		|	coupling		|

