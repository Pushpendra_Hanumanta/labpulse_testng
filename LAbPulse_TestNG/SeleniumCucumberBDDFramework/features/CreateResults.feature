@CucumberHooks
Feature: Create Results

@TestCase5
Scenario Outline: Create multiple Results by adding proper data set
    Given User is on Home Page
    When User select Data Analysis to create result
    Then User Enters '<ExperimentalDrug>' and '<Strains>' and '<AnimalName>' and '<Value>' 

Examples:    
|ExperimentalDrug	|Strains			|AnimalName		|Value		|
|		LDA1		|	Balb/c			|	Mice		|	0.5		|
