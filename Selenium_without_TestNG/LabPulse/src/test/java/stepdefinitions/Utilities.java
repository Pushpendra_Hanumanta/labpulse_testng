package stepdefinitions;

public class Utilities {
	
	public String driverPath = "C:\\chromedriver.exe";
	public String appURL = "https://login.salesforce.com";
	
	public String username_ID = "username";
	public String password_ID = "password";
	public String rememberMe_Xpath = "//*[@id=\"rememberUn\"]";
	public String loginButton_ID = "Login";
	
	public String appLauncher_Xpath = "//*[@id=\"oneHeader\"]/div[3]/div/div[1]/div[1]/nav/button";
	public String select_Labpulse = "//*[@id=\"02u2v0000010qrLAAQ\"]/div/div/div";
	
	//Create lab project(Lab Info)
	public String createProject ="//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div/div[1]/div[1]/div[1]/article/header/div[2]/p";
	public String projectTitle_textBox_Xpath = "/html/body/div[5]/div[2]/div/div[2]/div/div[2]/div[2]/form/lightning-input-field[1]/lightning-input/div/input";
	public String objective_textBox_Xpath = "//*[@id=\"input-101\"]";
	public String sponsor_textBox = "//*[@id=\"input-105\"]";
	public String budget_textBox = "//*[@id=\"input-108\"]";
	public String startDate = "//*[@id=\"input-94\"]";
	public String endDate = "//*[@id=\"input-98\"]";
	
	//New Account window(Vendor/sponsor/Laboratory)
	public String Sponsor_TB = "//*[@id=\"770:0\"]";
	public String new_acc_sponsor = "//*[@id=\"798:0\"]";
	public String sponsor_checkbox = "//*[@id=\"content_1206:0\"]/div/div/div[1]/fieldset/div[2]/div[5]/label/div[2]/span";
	public String next_button="/html/body/div[5]/div[2]/div[3]/div[2]/div/div[3]/div/button[2]";
	
	public String AccName_Textbox= "//*[@id=\"94:1229;a\"]";
	public String Save_button = "/html/body/div[5]/div[2]/div[3]/div[2]/div/div[3]/div/button[3]/span";
	public String select_Sponsor = "//*[@id=\"2032:0\"]/div/table/tbody/tr/td[1]";
	public String next_button_ProjectInfo = "//button[@name='save']";

}
