package stepdefinitions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateProject extends Utilities
{

//==================================================LogIn==================================================================================
	public static WebDriver driver;

	@Given("^user is on login window$")
	public void user_is_on_login_window()
	{
		System.setProperty("webdriver.chrome.driver", driverPath);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(appURL);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@When("^enters '(.*)' and '(.*)'$")
	public void enters_username_and_Password(String arg1, String arg2)
	{
		driver.findElement(By.id(username_ID)).sendKeys(arg1);
		driver.findElement(By.id(password_ID)).sendKeys(arg2);
	}

	@Then("^click on login$")
	public void click_on_login() throws Exception 
	{
		// Clicking on remember me
		driver.findElement(By.xpath(rememberMe_Xpath)).click();

		// Clicking on Login button
		driver.findElement(By.id(loginButton_ID)).click();
		Thread.sleep(9000);

//		 Robot robot = new Robot(); robot.mouseMove(350,150); Thread.sleep(3000); int
//		 mask = InputEvent.BUTTON1_DOWN_MASK; robot.mousePress(mask);
//		 robot.mouseRelease(mask);
		  
		 
	}
//=======================================================Create Project============================================================

	@Given("^User is on Labpulse home page$")
	public void user_is_on_Labpulse_home_page() throws Exception 
	{
		// Click on app launcher
		// driver.findElement(By.xpath(appLauncher_Xpath)).click();

		// choose labpulse project
		// driver.findElement(By.xpath(select_Labpulse)).click();
		Thread.sleep(3000);
	}

//======================================================================================================================
/*	
	  
	  @When("^Enter on lab project flow$") 
	  public void enter_on_lab_project_flow()throws Exception
	  { 
		  Robot r=new Robot(); r.keyPress(KeyEvent.VK_ESCAPE);
		  r.keyRelease(KeyEvent.VK_ESCAPE);
	  
		  driver.findElement(By.xpath(createProject)).click(); 
	  }
	  
	  
	  @Then("^Enters '(.*)' and '(.*)' and '(.*)' and '(.*)'$") 
	  public void enters_ProjectTitle_and_Sponsor_and_Budget_and_Objective(String s1, String s2, String s3, String s4 ) throws AWTException, Exception
	  {
		  driver.findElement(By.xpath(projectTitle_textBox_Xpath)).sendKeys(s1);
		  driver.findElement(By.xpath(budget_textBox)).sendKeys(s3);
		  driver.findElement(By.xpath(objective_textBox_Xpath)).sendKeys(s4);
	  
		  Robot r=new Robot(); // r.keyPress(KeyEvent.VK_ESCAPE); //
		  r.keyRelease(KeyEvent.VK_ESCAPE);
		  driver.findElement(By.xpath(sponsor_textBox)).sendKeys(s2);
		  Thread.sleep(3000); r.keyPress(KeyEvent.VK_ENTER);
		  r.keyRelease(KeyEvent.VK_ENTER); Thread.sleep(4000);
		  r.keyPress(KeyEvent.VK_TAB); 
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB); 
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB); 
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB); 
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB);
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_ENTER);
		  r.keyRelease(KeyEvent.VK_ENTER);
		  //driver.findElement(By.xpath(select_Sponsor)).click();
	  }
	*/ 
//--------------------------------------------------------------------------------------
	 
/*
	 //driver.findElement(By.xpath(Sponsor_TB)).click();
	  	r.keyPress(KeyEvent.VK_TAB); r.keyRelease(KeyEvent.VK_TAB);
	  	r.keyPress(KeyEvent.VK_TAB); r.keyRelease(KeyEvent.VK_TAB);
	  	r.keyPress(KeyEvent.VK_TAB); r.keyRelease(KeyEvent.VK_TAB);
	  
		driver.findElement(By.xpath(new_acc_sponsor)).click();
	  //r.keyPress(KeyEvent.VK_ENTER); //r.keyRelease(KeyEvent.VK_ENTER);
	 
	 	driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	  	driver.findElement(By.xpath(sponsor_checkbox)).click();
	 	driver.findElement(By.xpath(next_button)).click(); Thread.sleep(3000);
	 
	  	driver.findElement(By.xpath(AccName_Textbox)).sendKeys("Pushpendra");
	  	driver.findElement(By.xpath(Save_button)).click();
	 
	*/ 
//------------------------------------------------------------------------------  

/*	
	@Then("^Choose StartDate EndDate and Status$")
	 public void choose_StartDate_EndDate_and_Status()
	 {
		 driver.findElement(By.xpath(startDate)).sendKeys("Oct 4, 2019");
		 driver.findElement(By.xpath(endDate)).sendKeys("Oct 4, 2019");
	 }
	  
	  @Then("^Save the project info$")
	  public void save_the_project_info() throws Exception
	  {
		  Thread.sleep(8000);
		  driver.findElement(By.xpath(next_button_ProjectInfo)).click();
	  }
	  
	  
	  @Given("^User is on Project Team window$") 
	  public void user_is_on_Project_Team_window() throws Exception 
	  { 
		  Thread.sleep(3000);
	  }
	  
	  @When("^Fill the inputs$") public void fill_the_inputs() {
	  
	  }
	  
	  
	  @Then("^Enter '(.*)' and '(.*)'$") public void
	  Enter_ProjectTeam_and_Researcher(String p, String r1) throws Exception
	  {
	  	  driver.findElement(By.xpath("//input[@id='input-138']")).sendKeys(p);
		  driver.findElement(By.xpath("//input[@id='input-142']")).sendKeys(r1);
	  
		  Robot r=new Robot(); Thread.sleep(2000); r.keyPress(KeyEvent.VK_ENTER);
		  r.keyRelease(KeyEvent.VK_ENTER); Thread.sleep(3000);
		  r.keyPress(KeyEvent.VK_TAB); r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB); r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB); r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB); r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB); r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_ENTER); r.keyRelease(KeyEvent.VK_ENTER); 
	  }
	  
	  
	  @Then("^Save the project team info$") 
	  public void Save_the_project_team_info() throws Exception
	  { 
		  Thread.sleep(3000);
		  driver.findElement(By.xpath("//*[@id=\"content_691:0\"]/div[2]/form/button")).click();
	  }
	  
	  
	  @Given("^User is on Experiment Design window$") 
	  public void user_is_on_Experiment_Design_window()
	  {
	  
	  }
	  
	  @When("^Fill the inputs for exp$")
	  public void fill_the_inputs_for_exp() throws Exception
	  { 
		  Thread.sleep(3000);
	  }
	  
	  @Then("^User Enters '(.*)' and '(.*)'$") 
	  public void user_Enters_ExperimentName_and_Protocol(String p12, String r12) throws Exception
	  {
		  driver.findElement(By.xpath("//input[@id='input-181']")).sendKeys(p12);
		  driver.findElement(By.xpath("//input[@id='input-197']")).sendKeys(r12);
	  
		  Robot r=new Robot();
		  Thread.sleep(2000);
		  r.keyPress(KeyEvent.VK_ENTER);
		  r.keyRelease(KeyEvent.VK_ENTER); 
		  Thread.sleep(3000);
		  
		  r.keyPress(KeyEvent.VK_TAB);
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB);
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB);
		  r.keyRelease(KeyEvent.VK_TAB);
		  r.keyPress(KeyEvent.VK_TAB);
		  r.keyRelease(KeyEvent.VK_TAB);
	  
		  r.keyPress(KeyEvent.VK_ENTER);
		  r.keyRelease(KeyEvent.VK_ENTER); 
	  }
	  
	  @Then("^Save the Experiment info$")
	  public void save_the_Experiment_info() throws Exception
	  { 
		  Thread.sleep(3000);
		  driver.findElement(By.xpath("//*[@id=\"content_691:0\"]/div[2]/form/button")).click();
	  }
	  
	  
	  @Given("^User is on Specimen Selection window$") public void
	  user_is_on_Specimen_Selection_window() 
	  {
	  
	  }
	  
	  @When("^Fill the inputs for Specimen Selection$") public void
	  fill_the_inputs_for_Specimen_Selection() 
	  {
	  
	  }
	  
	  @Then("^Required input is '(.*)'$") 
	  public void required_input_is_SpecimenName(String qk) throws Exception 
	  {
		  driver.findElement(By.xpath("//*[@id=\"input-239\"]")).sendKeys(qk);
	  
		  WebElement dd=driver.findElement(By.xpath("//input[@name='Nature__c']"));
		  dd.click(); dd.sendKeys(Keys.DOWN);
		  dd.sendKeys(Keys.DOWN);
		  dd.sendKeys(Keys.ENTER);
	  
		  driver.findElement(By.xpath("//input[@id=\"input-241\"]")).sendKeys("Oct 1, 2019"); //WebElement
		  dd=driver.findElement(By.xpath("//input[@id=\"input-241\"]"));
		  //dd2.click(); //dd2.sendKeys(Keys.ENTER);
	  
		  WebElement dd1=driver.findElement(By.xpath("//input[@name='Location__c']"));
		  dd1.click(); 
		  dd1.sendKeys(Keys.DOWN); 
		  dd1.sendKeys(Keys.DOWN);
		  dd1.sendKeys(Keys.ENTER); 
	  }
	  
	  
	  
	  @Then("^Save the Specimen info$")
	  public void save_the_Specimen_info() throws Exception
	  { 
		  Thread.sleep(4000);
		  driver.findElement(By.xpath("//*[@name='save']")).click();
	  }

	  */
//============================================Create Result=========================================================================
/*
	  
	
	@Given("^User is on Labpulse Home Page$")
	public void user_is_on_Labpulse_Home_Page() throws Exception
	{
		Robot robot = new Robot();
		robot.mouseMove(350, 150);
		Thread.sleep(3000);
		int mask = InputEvent.BUTTON1_DOWN_MASK;
		robot.mousePress(mask);
		robot.mouseRelease(mask);
	}

	@When("^User select Data Analysis$")
	public void user_select_Data_Analysis() throws Exception
	{
		Robot r3 = new Robot();

		driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div/div[2]/div[2]/article/div/div/center/table/tr[1]/td[2]/span/div")).click();
		Thread.sleep(5000);
		// driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		r3.keyPress(KeyEvent.VK_ESCAPE);
		r3.keyRelease(KeyEvent.VK_ESCAPE);

		driver.findElement(By.xpath("//div//input [@class=\"slds-input slds-combobox__input\"][@placeholder='Search Lab Projects...']")).sendKeys("A");
		Thread.sleep(2000);
		r3.keyPress(KeyEvent.VK_ENTER);
		r3.keyRelease(KeyEvent.VK_ENTER);

		driver.findElement(By.xpath("//div//input [@class=\"slds-input slds-combobox__input\"][@placeholder='Search Experiments...']")).sendKeys("DNA");
		Thread.sleep(1000);
		r3.keyPress(KeyEvent.VK_ENTER);
		r3.keyRelease(KeyEvent.VK_ENTER);
		
//		  WebElement dd5=driver.findElement(By.xpath("//div//input [@class=\\\"slds-input slds-combobox__input\\\"][@placeholder='Search Experiments...']"));
//		  dd5.click(); 
//		  dd5.sendKeys(Keys.DOWN); 
//		  dd5.sendKeys(Keys.DOWN);
//		  dd5.sendKeys(Keys.ENTER); 
		
		

		//driver.findElement(By.xpath("//a[@class='outputLookupLink slds-truncate outputLookupLink-a072v000014wvAMAAY-58:1084;a forceOutputLookup']")).click();
		//WebElement dd1=driver.findElement(By.xpath("//a[@class='outputLookupLink slds-truncate outputLookupLink-a072v000014wvAMAAY-58:1084;a forceOutputLookup']"));
//		  dd1.click(); 
//		  dd1.sendKeys(Keys.DOWN); 
//		  dd1.sendKeys(Keys.DOWN);
//		  dd1.sendKeys(Keys.ENTER); 


	}

	
	@Then("^User Enters '(.*)' and '(.*)' and '(.*)' and '(.*)'$")
	public void user_Enters_ExperimentalDrug_and_Strains_and_AnimalName_and_Value(String Ex, String Str, String An,String Val)
	{
		String x;

		for (int m = 1; m <= 4; m++) {
			for (int k = 1, j = 0; k <= 4; k++) {
				for (int l = 1; l <= 60; l++) {
					for (int i = 1; i <= 4; i = i + 1) {

						String animal = ("Mice" + i);

						String strain[] = { "Balb/c", "Out bred", "C/57", "Gg4", "Ogk" };

						String drug = ("LDA" + k);

						String value = ("0.25" + i);

						j++;

						x = "(//td/input [@class=\"slds-input input uiInput uiInputText uiInput--default uiInput--input\"])["+ j + "]";
						driver.findElement(By.xpath(x)).sendKeys(drug);
						System.out.println(x + ": " + value);
						j++;

						x = "(//td/input [@class=\"slds-input input uiInput uiInputText uiInput--default uiInput--input\"])["+ j + "]";
						driver.findElement(By.xpath(x)).sendKeys(strain[l]);
						System.out.println(x + ": " + value);
						j++;

						x = "(//td/input [@class=\"slds-input input uiInput uiInputText uiInput--default uiInput--input\"])["+ j + "]";
						driver.findElement(By.xpath(x)).sendKeys(animal);
						System.out.println(x + ": " + value);
						j++;

						x = "(//td/input [@class=\"slds-input input uiInput uiInputText uiInput--default uiInput--input\"])["+ j + "]";
						System.out.println(x + ": " + value);
						driver.findElement(By.xpath(x)).sendKeys(value);
						driver.findElement(By.xpath("(//*[contains(@data-key,'add')])[2]")).click();
						
						

					}
				}
			}
		}
		
		driver.findElement(By.xpath("//button[@class='slds-button slds-button_brand slds-align_absolute-center']")).click();
	}

*/
	
//====================================================Generate Report==========================================================================	
 
	
	@Given("^User is on Labpulse Home Page$") public void
	  user_is_on_Labpulse_Home_Page() throws Exception
	  {
		  Robot robot = new Robot();
		  robot.mouseMove(350,150); Thread.sleep(3000); int mask =
		  InputEvent.BUTTON1_DOWN_MASK; robot.mousePress(mask);
		  robot.mouseRelease(mask);	  
	  }
	  
	 @When("^User select Data Analysis$") 
	 public void user_select_Data_Analysis()throws Exception
	 {
		 Robot r3 = new Robot(); r3.keyPress(KeyEvent.VK_ESCAPE);
		 r3.keyRelease(KeyEvent.VK_ESCAPE);
	  
		 driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div/div[2]/div[2]/article/div/div/center/table/tr[1]/td[2]/span/div")).click();
	  
		 driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	  
	  }
	  
	  
	  
	  @Then("^Click on View Data$") 
	  public void click_on_View_Data() throws Exception
	  {
		  driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div[2]/div/div/button[2]")).click();
		  Thread.sleep(5000);
	  }
	  
	  @Then("^User Enters '(.*)' and '(.*)' and '(.*)'$")
	  public void user_Enters_Researcher_and_Project_and_Experiment(String Res, String Pro,String Ex) throws Exception 
	  {
		  Robot r4 = new Robot(); r4.keyPress(KeyEvent.VK_ESCAPE);
		  r4.keyRelease(KeyEvent.VK_ESCAPE);
	  
		  driver.findElement(By.xpath("//input[@name='Researcher']")).sendKeys(Res);
		  Thread.sleep(2000); r4.keyPress(KeyEvent.VK_ENTER);
		  r4.keyRelease(KeyEvent.VK_ENTER);
	  
	  
		  // WebElement dd1=driver.findElement(By.xpath("//input[@id='input-96']")); 
		  //dd1.click();
		  // dd1.sendKeys(Keys.DOWN); // dd1.sendKeys(Keys.DOWN);
		  //dd1.sendKeys(Keys.ENTER);
		  //driver.findElement(By.xpath("//*[@id=\"input-96\"]")).sendKeys(Pro);
	  
		  driver.findElement(By.xpath("//input[@name='Project']")).click();
		  Thread.sleep(2000); r4.keyPress(KeyEvent.VK_ENTER);
		  r4.keyRelease(KeyEvent.VK_ENTER);
	  
		  driver.findElement(By.xpath("//input[@name='Experiment']")).sendKeys(Ex);
		  Thread.sleep(2000); r4.keyPress(KeyEvent.VK_ENTER);
		  r4.keyRelease(KeyEvent.VK_ENTER); 
	  }
	  
	  @Then("^Search$") 
	  public void search() 
	  {
		  driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div[3]/div[1]/div/div[1]/div/form/div/div[4]/button[1]")).click(); 
	  }
	  
	  @Then("^Generate Report$") 
	  public void generate_Report() throws Exception 
	  {
		  driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div[3]/div[1]/div/div[1]/div/form/div/div[4]/button[2]")).click(); 
	  }
	  
	 
//===========================================================================================================================

}