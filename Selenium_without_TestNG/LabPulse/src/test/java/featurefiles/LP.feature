@Login
Feature: Login into LabPulse
  
  Scenario Outline: Login as a authenticated user
    Given user is on login window
    When enters '<username>' and '<password>'
    Then click on login
               
Examples:
|username							|password		|
|pushpendra@focalcxm.com.testfocal	|temp*123		| 

#====================================================CREATE PROJECT===================================================================
#Scenario Outline: Create Lab Project
#    Given User is on Labpulse home page
#    When Enter on lab project flow
#   Then Enters '<ProjectTitle>' and '<Sponsor>' and '<Budget>' and '<Objective>'
#    Then Choose StartDate EndDate and Status  
#    Then Save the project info
#   
#Examples:
#|ProjectTitle											 |Sponsor		| Budget	|Objective																									|
#|Agarose Gel Electrophoresis for resolving DNA fragments|GEF			|564	 	|seperation of DNA fragments was done by electrophoresis through agarose gels  at 70 volts in TBE.			|
#    
#Scenario Outline: Create Project Team
#    Given User is on Project Team window
#    When Fill the inputs
#    Then Enter '<ProjectTeam>' and '<Researcher>'
#   	Then Save the project team info
#   
#Examples:    
#|ProjectTeam		|   Researcher		|
#|Agarose_team		|	Pushpendara		|
#
#Scenario Outline: Create Experiments
#    Given User is on Experiment Design window
#    When Fill the inputs for exp
#    Then User Enters '<ExperimentName>' and '<Protocol>' 
#   	Then Save the Experiment info
#   
#Examples:    
#|ExperimentName				| Protocol			|
#| DNA fragmentation			|Cloning Protocol	|
#
#Scenario Outline: Create Specimen
#    Given User is on Specimen Selection window
#    When Fill the inputs for Specimen Selection
#    Then Required input is '<SpecimenName>' 
#    Then Save the Specimen info
#   
#Examples:    
#|SpecimenName	|
#|	leo			|


#===============================================CREATE RESULT===============================================================================

#Scenario Outline: Create Results
#    Given User is on Labpulse Home Page
#    When User select Data Analysis
#    Then User Enters '<ExperimentalDrug>' and '<Strains>' and '<AnimalName>' and '<Value>' 
#
#Examples:    
#|ExperimentalDrug	|Strains			|AnimalName		|Value		|
#|		LDA1		|	Balb/c			|	Mice		|	0.5		|


#================================================Generate Report============================================================================

Scenario Outline: Generate Report
    Given User is on Labpulse Home Page
    When User select Data Analysis
    Then Click on View Data
    Then User Enters '<Researcher>' and '<Project>' and '<Experiment>'
    Then Search 
    Then Generate Report
    Examples:    
|Researcher	|Project		|Experiment		|
|push		|Enzyme		|	coupling			|
