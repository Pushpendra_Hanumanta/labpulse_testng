package stpdef;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;


public class Beat {
	public static WebDriver driver;
//	public String username="de.agent1@merck.com.gbld36dev1";
	
	@Given("^user is on homepage$")
	public void openBrowser()
	{
		System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://test.salesforce.com/");
	}
	
	@And("^user navigates to Login Page$")
	public void loginpage()
	{
		
	}

	@And("^enter '(.*)' and '(.*)' fields$")
	public void enterUsername(String Username,String Password )
	{
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.findElement(By.id("username")).sendKeys(Username);
		driver.findElement(By.id("password")).sendKeys(Password);	
		}
}
