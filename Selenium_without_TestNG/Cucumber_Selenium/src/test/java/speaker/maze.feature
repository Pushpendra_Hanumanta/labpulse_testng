@Login
Feature: Login Feature
  
  As a user, I want to Signup and login to Baton Folio Non CRM flow
  so that I can make the Configurations, Teams creation, Projects and Assessments.

 @BatonHomePage
 Scenario: Land on the Baton Home Page
	Given user navigate to Baton homepage
 	When user clicks on Folio app icon
 	Then app should navigate to SignIn screen

 @BatonSignUp_InValidCredentials
 Scenario Outline: Sign Up with an invalid credentials
    Given the user is on the Sign in screen
    When user clicks on Sign Up button, navigates to Sign Up screen
    And user enter an invalid First Name <firstname>, Last Name <lastname>, Organization <organization>, Username(Email) <username>
    And click Sign Up button
    And app pops "Signing Up ..." message on the screen
	Then user should NOT be able to login successfully and alerted with error message

	Examples: 
      | firstname	| lastname	| organization	| username			| errormessage 						|
      | Blank		| Blank		| Blank 		| Blank			 	| no action for the sign up button 	|
      | Blank		| CXM1		| FocalCXM 		| test@focalcxm.com	| no action for the sign in button 	|
      | focal1		| Blank		| FocalCXM 		| test@focalcxm.com	| no action for the sign in button 	|
      | focal2		| CXM2		| Blank 		| test@focalcxm.com	| no action for the sign in button 	|
      | focal3		| CXM3		| FocalCXM 		| Blank				| no action for the sign in button 	|
      | focal4		| CXM4 		| FocalCXM 		| test@focalcxm.co 	| INTERNAL_ERROR					|
      | focal4		| CXM4 		| FocalCXM 		| test@focalcxm 	| Please enter valid email.			|
      | focal5 		| CXM5 		| FocalCXM 		| test.com 			| Please enter valid email.			| 
      | focal6		| CXM6 		| FocalCXM 		| test@.com 		| Please enter valid email.			|
      | focal7		| CXM7 		| FocalCXM 		| focalcxm.com 		| Please enter valid email.			|

 @BatonSignUp_RegisteredUser
 Scenario Outline: Sign Up with already registered credentials
    Given the user is on the Sign in screen
    When user clicks on Sign Up button, navigates to Sign Up screen
    And user enters First Name <firstname>, Last Name <lastname>, Organization <organization>, Username(Email) <username>
    And click Sign Up button
    And app pops "Signing Up ..." message on the screen
	Then user should NOT be able to login successfully and alerted with error message
	And screen is navigated to Sign In, when user clicks on alert message Ok button 

	Examples: 
      | firstname	| lastname	| organization	| username			|
      | focal		| CXM		| FocalCXM 		| test@focalcxm.com |

 @BatonSignUp_ValidCredentials
 Scenario Outline: Sign Up with a valid credential
    Given the user is on the Sign in screen
    When user clicks on Sign Up button, navigates to Sign Up screen
    And user enters First Name <firstname>, Last Name <lastname>, Organization <organization>, Username(Email) <username> which is already registered earlier
    And click Sign Up button
    And app pops "Signing Up ..." message on the screen
	Then user should be able to login successfully 
	And navigate to Baton Dashboard screen

    Examples: 
      | firstname	| lastname	| organization	| username			|
      | focal		| CXM		| FocalCXM 		| test@focalcxm.com	|

 @BatonSignin_InValidCredentials
 Scenario Outline: Login with an invalid credential
    Given the user navigate to Baton homepage
    When user click on Folio icon, navigates to SignIn screen
    And user enter an invalid username <username> and password <password>
    And click on Sign In button
    And app pops "Logging in ..." message on the screen
	Then user should NOT be able to login successfully 
	And alerted with the error message <errormessage>

    Examples: 
      | username 					| password           			| errormessage 						|
      | BlankUsername				| BlankPassword 				| no action for the sign in button 	|
      | WrongUsername				| Blank					 		| Please enter valid email.			|
      | CorrectUsername				| Blank 						| no action for the sign in button  |
      | Blank						| CorrectPassword				| no action for the sign in button  |
      | WrongUsername 				| CorrectPassword			 	| Authentication Failure			|
      | CorrectUsername 			| WrongPassword				 	| Incorrect username or password.	|
      | CorrectUsername				| Blank			 				| no action for the sign in button 	|

 @BatonSignin_ValidCredentials
 Scenario Outline: Sign In with a valid credential
    Given the user navigate to Baton homepage
    When user click on Folio icon, navigates to SignIn screen
    And user enters valid username <username> and password <password>
    And click on Sign In button
    And app pops "Logging in ..." message on the screen
	Then user should be able to login successfully 
	And navigate to Baton Dashboard screen

    Examples: 
      | username 					| password |
      | girish.focalcxm@outlook.com | temp1234 |