package stepDefinitionFiles;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps  {
	public static WebDriver driver;

@Given("^user is on login page$")
public void user_is_on_homepage()
{
	System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe");
    driver = new ChromeDriver();
    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    driver.get("https://test.salesforce.com");
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
}


@When("^user enters '(.*)' and '(.*)'$")
	public void user_enters_username_and_Password(String arg1, String arg2)
	{
	driver.findElement(By.id("username")).sendKeys(arg1);
    driver.findElement(By.id("password")).sendKeys(arg2);
    
	}


@Then("^click on login button$")
public void click_on_login_button() throws Exception 
{
	driver.findElement(By.xpath("//*[@id=\"rememberUn\"]")).click();
			
	driver.findElement(By.id("Login")).click(); 
	Thread.sleep(60000);
	
}


@Then("^enter verification code$")
public void enter_verification_code()
{

	driver.findElement(By.id("save")).click();
	System.out.println("Login completed");
	
}


@Then("^user is on home page$")
public void user_is_on_home_page(){
	Alert alert=driver.switchTo().alert();
	driver.switchTo().alert().dismiss();
	System.out.println("Welcome to GICC Home");
}


@Then("^Click on Aerrow$")
public void click_on_Aerrow()
{
	driver.findElement(By.xpath("//*[@id=\"oneHeader\"]/div[3]/div/div[1]/div[3]/div/button/lightning-primitive-icon/svg")).click();

}

@Then("^Click on Account Search$")
public void click_on_Account_Search()
{
	driver.findElement(By.xpath("//*[@id=\"navMenuList\"]/div/ul/li[1]/div/a/span[2]/span")).click();
}

@Then("^Enters '(.*)' '(.*)' '(.*)' '(.*)' '(.*)' and '(.*)'$")
public void enters_LastName_FirstName_City_PIN_Phone_and_Email(String LN, String FN, String City, String pin, String ph, String mail) 
{
	
	driver.findElement(By.id("input-485")).sendKeys(LN);
	driver.findElement(By.id("input-486")).sendKeys(FN);
	driver.findElement(By.id("input-487")).sendKeys(City);
	driver.findElement(By.id("input-488")).sendKeys(pin);
	driver.findElement(By.id("input-489")).sendKeys(ph);
	driver.findElement(By.id("input-490")).sendKeys(mail);
}

@Then("^Select a district$")
public void select_a_district()
{

	Select dropdown = new Select(driver.findElement(By.xpath("//*[@id=\"28:4134;a\"]")));  
	dropdown.selectByVisibleText("NO"); 
	
}

@Then("^Click on Search$")
public void click_on_Search() 
{
	driver.findElement(By.xpath("//*[@id=\"one\"]/div[1]/div[2]/div[8]/button/lightning-primitive-icon")).click();
	
}
}