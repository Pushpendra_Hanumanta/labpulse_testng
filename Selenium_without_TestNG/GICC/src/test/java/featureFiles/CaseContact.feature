Feature: Objects in GICC
  
  Scenario Outline: Verify the cases
    Given User is on Home page
    When Click on Aerrow
    Then Click on Case Contacts
    Then Click on New button
    Then Enters <ContactLastName> <ContactFirstName> <ContactCity> <PIN> <Address>
    Then  Select a consent for followup
    Then Click on Search
    
Examples:
|ContactLastName|ContactFirstName|ContactCity|PIN|Address|
|test*|test*||||    