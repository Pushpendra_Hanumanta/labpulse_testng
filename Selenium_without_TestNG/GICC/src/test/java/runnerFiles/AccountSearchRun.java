package runnerFiles;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
features = "E:\\CMD\\GICC\\src\\test\\java\\featureFiles\\AccountSearch.feature"
,glue= {"stepDefinitionFiles\\AccountSearchStep"}
)

public class AccountSearchRun {

}
