@Login
Feature: Login into GICC
  
  Scenario Outline: Login as a authenticated user
    Given user is on login page
    When user enters '<username>' and '<password>'
    Then click on login button
    Then enter verification code
    Then user is on home page
    
Examples:
|username					   |password|
|de.agent1@merck.com.gbld36dev1|GICC2019|   
    
@AccountSearch
Scenario Outline: Searching the Accounts
    Then Click on Aerrow
    Then Click on Account Search
    Then Enters '<LastName>' '<FirstName>' '<City>' '<PIN>' '<Phone>' and '<Email>' 
    Then  Select a district
    Then Click on Search
    
Examples:
|LastName|FirstName|City|PIN|Phone|Email|    
|test*   |test*    |    |   |     |     |