@Login
Feature: Login into Folio
  
  Scenario Outline: Login as an authenticated user
    Given Folio Login Window
    When Entering '<username>' and '<password>'
    Then Clicking on Login
    Then Choosing admin console and Creating Team
    Then Creating Project
    Then Creating Assessment
    Then Creating Workshop
    Then Folio Home Page
    Then Configure Settings
    
    
Examples:
|username					|password	|
|girish.focalcxm@outlook.com|temp1234	| 