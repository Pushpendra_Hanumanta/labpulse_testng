package folioSteps;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FSteps 
	{
		WebDriver driver;		
		@Given("^Folio Login Window$")
		public void folio_Login_Window() throws Exception
			{
			//Opening browser	
				System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe");
			    driver = new ChromeDriver();
			    
			//Opening application    
			    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			    driver.get("https://ci.focalcxm.com:3443/baton-client/");
			    driver.manage().window().maximize();
			    Thread.sleep(10000);
			
			//Selecting Folio    
			    driver.findElement(By.xpath("//img[contains(@src,'./assets/images/folio_big.png')]")).click(); 
			    Thread.sleep(8000);
			}

		@When("^Entering '(.*)' and '(.*)'$")
		public void Entering_username_and_Password(String arg1, String arg2) throws Exception
			{	
			//Entered username and password
				driver.findElement(By.id("emailfield")).sendKeys(arg1);
				driver.findElement(By.id("password")).sendKeys(arg2);
			
			//Click on Oracle Logo
				//driver.findElement(By.xpath("//img[contains(@src,'./assets/images/signup/oracle.png')]")).click();
				//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);		
			}

		@Then("^Clicking on Login$")
		public void clicking_on_Login() throws Exception
			{	
			//Clicking on sign in button
				driver.findElement(By.xpath("//button[text()[contains(.,'Sign In')]]")).sendKeys(Keys.ENTER);
				Thread.sleep(5000);
			}

		@Then("^Choosing admin console and Creating Team$")
		public void choosing_admin_console_and_Creating_Team() throws Exception
			{
			//Clicking on Admin consol button
				driver.findElement(By.xpath("/html/body/app-root/dashboard-page/div/mat-card/div/div[2]/button[2]")).click();
				Thread.sleep(10000);
				
			//Clicking on Menu button
				driver.findElement(By.xpath("/html/body/app-root/div/div/app-navigation-layout/div/mat-toolbar/button/span/mat-icon")).click();
				Thread.sleep(4000);
/*
				
			//Clicking on Teams button	
				driver.findElement(By.xpath("/html/body/app-root/div/div/app-navigation-layout/div/mat-sidenav-container/mat-sidenav/div/mat-nav-list[1]/a/div")).click();
				
				
			//Clicking on Add Team button	
				Thread.sleep(6000);
				driver.findElement(By.xpath("//button[@class='mat-button-ripple mat-ripple']"));
				Actions actions = new Actions(driver);
			    actions.moveToElement(driver.findElement(By.xpath("//button[@class='mat-button-ripple mat-ripple']"))).click().perform();
				//driver.findElement(By.xpath("/html/body/app-root/div/div/app-navigation-layout/div/mat-sidenav-container/mat-sidenav-content/app-teams/div/div[1]/button/span")).click();
				
//				WebElement selectMenuOption = driver.findElement(By.xpath("//button[@class='mat-raised-button mat-accent']"));
//			     selectMenuOption.click();
//			     System.out.println("Selected 'Alternative' from Menu");
								

			//Enter team name
				driver.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).sendKeys("TeamName");
				
			//Selecting admin	
				Select drpAdmin = new Select(driver.findElement(By.xpath("//*[@id=\"mat-select-1\"]/div/div[1]/span")));
				drpAdmin.selectByVisibleText("ANTARCTICA");
				drpAdmin.selectByIndex(3);
				//driver.findElement(By.xpath("//*[@id=\"mat-select-1\"]/div/div[1]/span"))
		*/
		
				}
	
		@Then("^Creating Project$")
		public void creating_Project() throws Exception
			{
			
		/*	
				driver.findElement(By.xpath("/html/body/app-root/div/div/app-navigation-layout/div/mat-sidenav-container/mat-sidenav/div/mat-nav-list[2]/a/div")).click();
				Thread.sleep(5000);
				driver.findElement(By.xpath("/html/body/app-root/div/div/app-navigation-layout/div/mat-sidenav-container/mat-sidenav-content/app-projects/div/div[1]/button")).click();
				
				//project name entered
				driver.findElement(By.xpath("//*[@id=\"mat-input-2\"]")).sendKeys("Mouseover");
				
				//Client name entered
				driver.findElement(By.xpath("//*[@id=\"mat-input-3\"]")).sendKeys("Dell");
				
				//Select Start date
				//driver.findElement(By.xpath("/html/body/app-root/div/div/app-navigation-layout/div/mat-sidenav-container/mat-sidenav-content/app-view-project/mat-card/div/div[2]/mat-form-field/div/div[1]/div[2]/mat-datepicker-toggle/button")).click();
				driver.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).click();
				driver.findElement(By.xpath("//*[@id=\"mat-datepicker-0\"]/div/mat-month-view/table/tbody/tr[3]/td[4]/div")).click();
				
				//Select End date
				//driver.findElement(By.xpath("/html/body/app-root/div/div/app-navigation-layout/div/mat-sidenav-container/mat-sidenav-content/app-view-project/mat-card/div/div[2]/div/mat-form-field/div/div[1]/div[2]/mat-datepicker-toggle/button")).click();
				driver.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).click();
				driver.findElement(By.xpath("//*[@id=\"mat-datepicker-1\"]/div/mat-month-view/table/tbody/tr[3]/td[5]/div")).click();
				
				//Select Team
				//driver.findElement(By.xpath("/html/body/app-root/div/div/app-navigation-layout/div/mat-sidenav-container/mat-sidenav-content/app-view-project/mat-card/div/div[3]/div/mat-form-field/div/div[1]/div"));
			
				//Click on Create Project
				driver.findElement(By.xpath("/html/body/app-root/div/div/app-navigation-layout/div/mat-sidenav-container/mat-sidenav-content/app-view-project/mat-card/div/div[4]/button[2]")).click();
				
		*/	
			}

		@Then("^Creating Assessment$")
		public void creating_Assessment()
			{

			}

		@Then("^Creating Workshop$")
		public void creating_Workshop() throws Exception
			{
		
			//Select workshop
				driver.findElement(By.xpath("/html/body/app-root/div/div/app-navigation-layout/div/mat-toolbar/button/span/mat-icon")).click();
				Thread.sleep(5000);
				
			//CLick on create worshop
				driver.findElement(By.xpath("/html/body/app-root/div/div/app-navigation-layout/div/mat-sidenav-container/mat-sidenav-content/app-workshop/app-workshop-list/div/div[1]/div[2]")).click();
				Thread.sleep(5000);			
			
			//Click on Start new
	 			driver.findElement(By.xpath("//*[@id=\"mat-option-0\"]/span")).click();
				
			//Enter Workshop name
				driver.findElement(By.xpath("/html/body/ngx-cool-dialog/div[2]/input")).sendKeys("143");
				
			//click on start
				driver.findElement(By.xpath("/html/body/ngx-cool-dialog/div[2]/div[2]/button[2]")).click();
				
			//enter to canvas
				
				WebElement wbCanvas = driver.findElement(By.xpath("//*[@id=\"myDiagramDiv\"]/canvas"));
				Actions actionBuilder=new Actions(driver);          
				Action drawOnCanvas=actionBuilder
				                .contextClick(wbCanvas)
				                .moveToElement(wbCanvas,8,8)
				                .clickAndHold(wbCanvas)
				                .moveByOffset(120, 120)
				                .moveByOffset(60,70)
				                .moveByOffset(-140,-140)
				                .release(wbCanvas)
				                .build();
				drawOnCanvas.perform();
				

			  /*  Actions builder = new Actions(driver);
			    Action drawAction = builder.moveToElement(element,135,15) //start points x axis and y axis. 
			              .click()
			              .moveByOffset(200, 60) // 2nd points (x1,y1)
			              .click()
			              .moveByOffset(100, 70)// 3rd points (x2,y2)
			              .doubleClick()
			              .build();
			    drawAction.perform();		
				
			*/
				//Click on Schedule
				driver.findElement(By.xpath("//*[@id=\"mat-option-1\"]/span")).click();
				
				//enter a title
				driver.findElement(By.xpath("//*[@id=\"mat-input-2\"]")).sendKeys("Avtar");
				
				//Enter start time
				//driver.findElement(By.xpath("")).click();
				 
					//Enter end time
				
				//enter the agenda
				driver.findElement(By.xpath("//*[@id=\'mat-input-5\']")).sendKeys("Test");
				
				//Enter atendees
				driver.findElement(By.xpath("//*[@id=\"mat-input-6\"]")).sendKeys("10");
				
				//click on save
				driver.findElement(By.xpath("/html/body/app-root/div/div/app-navigation-layout/div/mat-sidenav-container/mat-sidenav-content/app-workshop/app-workshop-create/div/div/mat-card/div[3]/div/div/button[1]")).click();
				
				
			}

		@Then("^Folio Home Page$")
		public void folio_Home_Page()
			{

			}

	
		@Then("^Configure Settings$")
		public void configure_Settings() throws Exception
			{/*
			//Enter on Configuration Settings	
				driver.findElement(By.xpath("//button[text()[contains(.,'Configure Settings')]]")).sendKeys(Keys.ENTER);
				Thread.sleep(8000);
				
			//Enter on Next	
				driver.findElement(By.xpath("//button[text()[contains(.,'Next')]]")).sendKeys(Keys.ENTER);
				Thread.sleep(5000);
				
			//	
				driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[2]/div[2]/button")).click();
				Thread.sleep(8000);
				
			//	
				driver.findElement(By.xpath("//*[@id=\"cdk-step-content-0-1\"]/form/div/button[2]")).click();
				
			//
				driver.findElement(By.xpath("//*[@id=\"cdk-step-content-1-1\"]/form/div/button[2]")).click();
				Thread.sleep(8000);
		
				//driver.findElement(By.xpath("//button[text()[contains(.,'Done')]]")).click();
				WebElement selectMenuOption = driver.findElement(By.xpath("//*[@id=\"container2\"]/div/span/button"));
				selectMenuOption.click();
				*/
			}

		@Then("^Folio home page$")
		public void folio_home_page()
			{
				System.out.println("Welcome to GICC Home");	
			}
	}
